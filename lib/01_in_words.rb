#Anastassia Bobokalonova
#April 7, 2017
#Note: The initial code (nums 0-100) was borrowed from Chris Pine's
#Learn to Program. The extended and refactored code is my own.

class Fixnum

  def in_words
    english_number(self)
  end

  def write_place(left)
    num_string = ""
    values = [[1000000000000, " trillion"], [1000000000, " billion"],
              [1000000, " million"], [1000, " thousand"],
              [100, " hundred"]]

    values.each do |place|
      write = left/(place[0])
      left = left - write*(place[0])

      if write > 0
        word = english_number(write)
        num_string += word + place[1]

        if left > 0
          num_string += " "
        end
      end
    end

    num_string
  end

  def english_number(number)
    return "zero" if number == 0

    onesPlace = ['one',     'two',       'three',    'four',     'five',
                 'six',     'seven',     'eight',    'nine']
    tensPlace = ['ten',     'twenty',    'thirty',   'forty',    'fifty',
                 'sixty',   'seventy',   'eighty',   'ninety']
    teenagers = ['eleven',  'twelve',    'thirteen', 'fourteen', 'fifteen',
                 'sixteen', 'seventeen', 'eighteen', 'nineteen']
    
    left = number
    num_string = write_place(left)
    left = number % 100

    write = left/10
    left  = left - write*10

    if write > 0
      if ((write == 1) and (left > 0))
        num_string += teenagers[left-1]
        left = 0
      else
        num_string += tensPlace[write-1]
      end

      if left > 0
        num_string += " "
      end
    end

    write = left
    left  = 0
    if write > 0
      num_string += onesPlace[write-1]
    end

    num_string
  end

end
